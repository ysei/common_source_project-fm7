//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDR_MENU1                       101
#define IDI_ICON1                       102
#define IDR_ACCELERATOR1                103
#define ID_ACCEL_SCREEN                 104
#define ID_ACCEL_MOUSE                  105
#define ID_BITMAP1                      106

#define IDD_VOLUME                      111
#define IDC_VOLUME_CAPTION0             112
#define IDC_VOLUME_CAPTION1             113
#define IDC_VOLUME_CAPTION2             114
#define IDC_VOLUME_CAPTION3             115
#define IDC_VOLUME_CAPTION4             116
#define IDC_VOLUME_CAPTION5             117
#define IDC_VOLUME_CAPTION6             118
#define IDC_VOLUME_CAPTION7             119
#define IDC_VOLUME_CAPTION8             120
#define IDC_VOLUME_CAPTION9             121
#define IDC_VOLUME_PARAM_L0             122
#define IDC_VOLUME_PARAM_L1             123
#define IDC_VOLUME_PARAM_L2             124
#define IDC_VOLUME_PARAM_L3             125
#define IDC_VOLUME_PARAM_L4             126
#define IDC_VOLUME_PARAM_L5             127
#define IDC_VOLUME_PARAM_L6             128
#define IDC_VOLUME_PARAM_L7             129
#define IDC_VOLUME_PARAM_L8             130
#define IDC_VOLUME_PARAM_L9             131
#define IDC_VOLUME_PARAM_R0             132
#define IDC_VOLUME_PARAM_R1             133
#define IDC_VOLUME_PARAM_R2             134
#define IDC_VOLUME_PARAM_R3             135
#define IDC_VOLUME_PARAM_R4             136
#define IDC_VOLUME_PARAM_R5             137
#define IDC_VOLUME_PARAM_R6             138
#define IDC_VOLUME_PARAM_R7             139
#define IDC_VOLUME_PARAM_R8             140
#define IDC_VOLUME_PARAM_R9             141
#define IDC_VOLUME_RESET                142

#define IDD_JOYSTICK                    151
#define IDC_JOYSTICK_CAPTION0           152
#define IDC_JOYSTICK_CAPTION1           153
#define IDC_JOYSTICK_CAPTION2           154
#define IDC_JOYSTICK_CAPTION3           155
#define IDC_JOYSTICK_CAPTION4           156
#define IDC_JOYSTICK_CAPTION5           157
#define IDC_JOYSTICK_CAPTION6           158
#define IDC_JOYSTICK_CAPTION7           159
#define IDC_JOYSTICK_CAPTION8           160
#define IDC_JOYSTICK_CAPTION9           161
#define IDC_JOYSTICK_CAPTION10          162
#define IDC_JOYSTICK_CAPTION11          163
#define IDC_JOYSTICK_CAPTION12          164
#define IDC_JOYSTICK_CAPTION13          165
#define IDC_JOYSTICK_CAPTION14          166
#define IDC_JOYSTICK_CAPTION15          167
#define IDC_JOYSTICK_PARAM0             168
#define IDC_JOYSTICK_PARAM1             169
#define IDC_JOYSTICK_PARAM2             170
#define IDC_JOYSTICK_PARAM3             171
#define IDC_JOYSTICK_PARAM4             172
#define IDC_JOYSTICK_PARAM5             173
#define IDC_JOYSTICK_PARAM6             174
#define IDC_JOYSTICK_PARAM7             175
#define IDC_JOYSTICK_PARAM8             176
#define IDC_JOYSTICK_PARAM9             177
#define IDC_JOYSTICK_PARAM10            178
#define IDC_JOYSTICK_PARAM11            179
#define IDC_JOYSTICK_PARAM12            180
#define IDC_JOYSTICK_PARAM13            181
#define IDC_JOYSTICK_PARAM14            182
#define IDC_JOYSTICK_PARAM15            183
#define IDC_JOYSTICK_RESET              184

#define ID_BUTTON                       1000

#define ID_RESET                        40001
#define ID_SPECIAL_RESET                40002
#define ID_AUTOKEY_START                40003
#define ID_AUTOKEY_STOP                 40004
#define ID_SAVE_STATE                   40005
#define ID_LOAD_STATE                   40006
#define ID_EXIT                         40007

#define ID_OPEN_DEBUGGER0               40011
#define ID_OPEN_DEBUGGER1               40012
#define ID_OPEN_DEBUGGER2               40013
#define ID_OPEN_DEBUGGER3               40014
#define ID_CLOSE_DEBUGGER               40015

#define ID_BOOT_MODE0                   40021
#define ID_BOOT_MODE1                   40022
#define ID_BOOT_MODE2                   40023
#define ID_BOOT_MODE3                   40024
#define ID_BOOT_MODE4                   40025
#define ID_BOOT_MODE5                   40026
#define ID_BOOT_MODE6                   40027
#define ID_BOOT_MODE7                   40028

#define ID_CPU_TYPE0                    40031
#define ID_CPU_TYPE1                    40032
#define ID_CPU_TYPE2                    40033
#define ID_CPU_TYPE3                    40034
#define ID_CPU_TYPE4                    40035
#define ID_CPU_TYPE5                    40036
#define ID_CPU_TYPE6                    40037
#define ID_CPU_TYPE7                    40038

#define ID_CPU_POWER0                   40041
#define ID_CPU_POWER1                   40042
#define ID_CPU_POWER2                   40043
#define ID_CPU_POWER3                   40044
#define ID_CPU_POWER4                   40045

#define ID_DIPSWITCH0                   40051
#define ID_DIPSWITCH1                   40052
#define ID_DIPSWITCH2                   40053
#define ID_DIPSWITCH3                   40054
#define ID_DIPSWITCH4                   40055
#define ID_DIPSWITCH5                   40056
#define ID_DIPSWITCH6                   40057
#define ID_DIPSWITCH7                   40058
#define ID_DIPSWITCH8                   40059
#define ID_DIPSWITCH9                   40060
#define ID_DIPSWITCH10                  40061
#define ID_DIPSWITCH11                  40062
#define ID_DIPSWITCH12                  40063
#define ID_DIPSWITCH13                  40064
#define ID_DIPSWITCH14                  40065
#define ID_DIPSWITCH15                  40066
#define ID_DIPSWITCH16                  40067
#define ID_DIPSWITCH17                  40068
#define ID_DIPSWITCH18                  40069
#define ID_DIPSWITCH19                  40070
#define ID_DIPSWITCH20                  40071
#define ID_DIPSWITCH21                  40072
#define ID_DIPSWITCH22                  40073
#define ID_DIPSWITCH23                  40074
#define ID_DIPSWITCH24                  40075
#define ID_DIPSWITCH25                  40076
#define ID_DIPSWITCH26                  40077
#define ID_DIPSWITCH27                  40078
#define ID_DIPSWITCH28                  40079
#define ID_DIPSWITCH29                  40080
#define ID_DIPSWITCH30                  40081
#define ID_DIPSWITCH31                  40082

#define ID_PRINTER_TYPE0                40086
#define ID_PRINTER_TYPE1                40087
#define ID_PRINTER_TYPE2                40088
#define ID_PRINTER_TYPE3                40089

#define ID_DEVICE_TYPE0                 40091
#define ID_DEVICE_TYPE1                 40092
#define ID_DEVICE_TYPE2                 40093
#define ID_DEVICE_TYPE3                 40094

#define ID_DRIVE_TYPE0                  40095
#define ID_DRIVE_TYPE1                  40096
#define ID_DRIVE_TYPE2                  40097
#define ID_DRIVE_TYPE3                  40098

#define ID_OPEN_FD1                     40101
#define ID_CLOSE_FD1                    40102
#define ID_WRITE_PROTECT_FD1            40103
#define ID_CORRECT_TIMING_FD1           40104
#define ID_IGNORE_CRC_FD1               40105
#define ID_RECENT_FD1                   40111 // 40111-40118
#define ID_D88_FILE_PATH1               40120
#define ID_SELECT_D88_BANK1             40121 // 40121-40199
#define ID_EJECT_D88_BANK1              40200

#define ID_OPEN_FD2                     40201
#define ID_CLOSE_FD2                    40202
#define ID_WRITE_PROTECT_FD2            40203
#define ID_CORRECT_TIMING_FD2           40204
#define ID_IGNORE_CRC_FD2               40205
#define ID_RECENT_FD2                   40211 // 40211-40218
#define ID_D88_FILE_PATH2               40220
#define ID_SELECT_D88_BANK2             40221 // 40221-40299
#define ID_EJECT_D88_BANK2              40300

#define ID_OPEN_FD3                     40301
#define ID_CLOSE_FD3                    40302
#define ID_WRITE_PROTECT_FD3            40303
#define ID_CORRECT_TIMING_FD3           40304
#define ID_IGNORE_CRC_FD3               40305
#define ID_RECENT_FD3                   40311 // 40311-40318
#define ID_D88_FILE_PATH3               40320
#define ID_SELECT_D88_BANK3             40321 // 40321-4399
#define ID_EJECT_D88_BANK3              40400

#define ID_OPEN_FD4                     40401
#define ID_CLOSE_FD4                    40402
#define ID_WRITE_PROTECT_FD4            40403
#define ID_CORRECT_TIMING_FD4           40404
#define ID_IGNORE_CRC_FD4               40405
#define ID_RECENT_FD4                   40411 // 40411-40418
#define ID_D88_FILE_PATH4               40420
#define ID_SELECT_D88_BANK4             40421 // 40421-40499
#define ID_EJECT_D88_BANK4              40500

#define ID_OPEN_FD5                     40501
#define ID_CLOSE_FD5                    40502
#define ID_WRITE_PROTECT_FD5            40503
#define ID_CORRECT_TIMING_FD5           40504
#define ID_IGNORE_CRC_FD5               40505
#define ID_RECENT_FD5                   40511 // 40511-40518
#define ID_D88_FILE_PATH5               40520
#define ID_SELECT_D88_BANK5             40521 // 40521-40599
#define ID_EJECT_D88_BANK5              40600

#define ID_OPEN_FD6                     40601
#define ID_CLOSE_FD6                    40602
#define ID_WRITE_PROTECT_FD6            40603
#define ID_CORRECT_TIMING_FD6           40604
#define ID_IGNORE_CRC_FD6               40605
#define ID_RECENT_FD6                   40611 // 40611-40618
#define ID_D88_FILE_PATH6               40620
#define ID_SELECT_D88_BANK6             40621 // 40621-40699
#define ID_EJECT_D88_BANK6              40700

#define ID_OPEN_FD7                     40701
#define ID_CLOSE_FD7                    40702
#define ID_WRITE_PROTECT_FD7            40703
#define ID_CORRECT_TIMING_FD7           40704
#define ID_IGNORE_CRC_FD7               40705
#define ID_RECENT_FD7                   40711 // 40711-40718
#define ID_D88_FILE_PATH7               40720
#define ID_SELECT_D88_BANK7             40721 // 40721-40799
#define ID_EJECT_D88_BANK7              40800

#define ID_OPEN_FD8                     40801
#define ID_CLOSE_FD8                    40802
#define ID_WRITE_PROTECT_FD8            40803
#define ID_CORRECT_TIMING_FD8           40804
#define ID_IGNORE_CRC_FD8               40805
#define ID_RECENT_FD8                   40811 // 40811-40818
#define ID_D88_FILE_PATH8               40820
#define ID_SELECT_D88_BANK8             40821 // 40821-40899
#define ID_EJECT_D88_BANK8              40900

#define ID_OPEN_QD1                     40901
#define ID_CLOSE_QD1                    40902
#define ID_RECENT_QD1                   40903 // 40903-40910

#define ID_OPEN_QD2                     40911
#define ID_CLOSE_QD2                    40912
#define ID_RECENT_QD2                   40913 // 40913-40920

#define ID_OPEN_CART1                   40921
#define ID_CLOSE_CART1                  40922
#define ID_RECENT_CART1                 40923 // 40923-40930

#define ID_OPEN_CART2                   40931
#define ID_CLOSE_CART2                  40932
#define ID_RECENT_CART2                 40933 // 40933-40940

#define ID_PLAY_TAPE                    40941
#define ID_REC_TAPE                     40942
#define ID_CLOSE_TAPE                   40943
#define ID_PLAY_BUTTON                  40944
#define ID_STOP_BUTTON                  40945
#define ID_FAST_FORWARD                 40946
#define ID_FAST_REWIND                  40947
#define ID_APSS_FORWARD                 40948
#define ID_APSS_REWIND                  40949
#define ID_PLAY_TAPE_SOUND              40951
#define ID_USE_WAVE_SHAPER              40952
#define ID_DIRECT_LOAD_MZT              40953
#define ID_TAPE_BAUD_LOW                40954
#define ID_TAPE_BAUD_HIGH               40955
#define ID_RECENT_TAPE                  40961 // 40961-40968

#define ID_OPEN_LASER_DISC              40971
#define ID_CLOSE_LASER_DISC             40972
#define ID_RECENT_LASER_DISC            40973 // 40973-40980

#define ID_LOAD_BINARY1                 40981
#define ID_SAVE_BINARY1                 40982
#define ID_RECENT_BINARY1               40983 // 40983-40990

#define ID_LOAD_BINARY2                 40991
#define ID_SAVE_BINARY2                 40992
#define ID_RECENT_BINARY2               40993 // 40993-41000

#define ID_SCREEN_REC60                 41001
#define ID_SCREEN_REC30                 41002
#define ID_SCREEN_REC15                 41003
#define ID_SCREEN_STOP                  41004
#define ID_SCREEN_CAPTURE               41005
#define ID_SCREEN_WINDOW1               41011
#define ID_SCREEN_WINDOW2               41012
#define ID_SCREEN_WINDOW3               41013
#define ID_SCREEN_WINDOW4               41014
#define ID_SCREEN_WINDOW5               41015
#define ID_SCREEN_WINDOW6               41016
#define ID_SCREEN_WINDOW7               41017
#define ID_SCREEN_WINDOW8               41018
#define ID_SCREEN_FULLSCREEN1           41021
#define ID_SCREEN_FULLSCREEN2           41022
#define ID_SCREEN_FULLSCREEN3           41023
#define ID_SCREEN_FULLSCREEN4           41024
#define ID_SCREEN_FULLSCREEN5           41025
#define ID_SCREEN_FULLSCREEN6           41026
#define ID_SCREEN_FULLSCREEN7           41027
#define ID_SCREEN_FULLSCREEN8           41028
#define ID_SCREEN_FULLSCREEN9           41029
#define ID_SCREEN_FULLSCREEN10          41030
#define ID_SCREEN_FULLSCREEN11          41031
#define ID_SCREEN_FULLSCREEN12          41032
#define ID_SCREEN_FULLSCREEN13          41033
#define ID_SCREEN_FULLSCREEN14          41034
#define ID_SCREEN_FULLSCREEN15          41035
#define ID_SCREEN_FULLSCREEN16          41036
#define ID_SCREEN_FULLSCREEN17          41037
#define ID_SCREEN_FULLSCREEN18          41038
#define ID_SCREEN_FULLSCREEN19          41039
#define ID_SCREEN_FULLSCREEN20          41040
#define ID_SCREEN_FULLSCREEN21          41041
#define ID_SCREEN_FULLSCREEN22          41042
#define ID_SCREEN_FULLSCREEN23          41043
#define ID_SCREEN_FULLSCREEN24          41044
#define ID_SCREEN_FULLSCREEN25          41045
#define ID_SCREEN_FULLSCREEN26          41046
#define ID_SCREEN_FULLSCREEN27          41047
#define ID_SCREEN_FULLSCREEN28          41048
#define ID_SCREEN_FULLSCREEN29          41049
#define ID_SCREEN_FULLSCREEN30          41050
#define ID_SCREEN_FULLSCREEN31          41051
#define ID_SCREEN_FULLSCREEN32          41052

#define ID_SCREEN_WINDOW_STRETCH1       41061
#define ID_SCREEN_WINDOW_STRETCH2       41062
#define ID_SCREEN_FULLSCREEN_STRETCH1   41063
#define ID_SCREEN_FULLSCREEN_STRETCH2   41064
#define ID_SCREEN_FULLSCREEN_STRETCH3   41065
#define ID_SCREEN_FULLSCREEN_STRETCH4   41066

#define ID_SCREEN_USE_D3D9              41071
#define ID_SCREEN_WAIT_VSYNC            41072

#define ID_SCREEN_MONITOR_TYPE0         41081
#define ID_SCREEN_MONITOR_TYPE1         41082
#define ID_SCREEN_MONITOR_TYPE2         41083
#define ID_SCREEN_MONITOR_TYPE3         41084
#define ID_SCREEN_MONITOR_TYPE4         41085
#define ID_SCREEN_MONITOR_TYPE5         41086
#define ID_SCREEN_MONITOR_TYPE6         41087
#define ID_SCREEN_MONITOR_TYPE7         41088

#define ID_SCREEN_CRT_FILTER            41091
#define ID_SCREEN_SCANLINE              41092
#define ID_SCREEN_ROTATE_0              41093
#define ID_SCREEN_ROTATE_90             41094
#define ID_SCREEN_ROTATE_180            41095
#define ID_SCREEN_ROTATE_270            41096

#define ID_SOUND_REC                    41101
#define ID_SOUND_STOP                   41102
#define ID_SOUND_FREQ0                  41111
#define ID_SOUND_FREQ1                  41112
#define ID_SOUND_FREQ2                  41113
#define ID_SOUND_FREQ3                  41114
#define ID_SOUND_FREQ4                  41115
#define ID_SOUND_FREQ5                  41116
#define ID_SOUND_FREQ6                  41117
#define ID_SOUND_FREQ7                  41118
#define ID_SOUND_LATE0                  41121
#define ID_SOUND_LATE1                  41122
#define ID_SOUND_LATE2                  41123
#define ID_SOUND_LATE3                  41124
#define ID_SOUND_LATE4                  41125

#define ID_SOUND_DEVICE_TYPE0           41131
#define ID_SOUND_DEVICE_TYPE1           41132
#define ID_SOUND_DEVICE_TYPE2           41133
#define ID_SOUND_DEVICE_TYPE3           41134
#define ID_SOUND_DEVICE_TYPE4           41135
#define ID_SOUND_DEVICE_TYPE5           41136
#define ID_SOUND_DEVICE_TYPE6           41137
#define ID_SOUND_DEVICE_TYPE7           41138

#define ID_SOUND_VOLUME                 41141

#define ID_CAPTURE_FILTER               41201
#define ID_CAPTURE_PIN                  41202
#define ID_CAPTURE_SOURCE               41203
#define ID_CAPTURE_CLOSE                41204
#define ID_CAPTURE_DEVICE1              41205
#define ID_CAPTURE_DEVICE2              41206
#define ID_CAPTURE_DEVICE3              41207
#define ID_CAPTURE_DEVICE4              41208
#define ID_CAPTURE_DEVICE5              41209
#define ID_CAPTURE_DEVICE6              41210
#define ID_CAPTURE_DEVICE7              41211
#define ID_CAPTURE_DEVICE8              41212

#define ID_INPUT_USE_DINPUT             41301
#define ID_INPUT_DISABLE_DWM            41302
#define ID_INPUT_JOYSTICK0              41303
#define ID_INPUT_JOYSTICK1              41304
#define ID_INPUT_JOYSTICK2              41305
#define ID_INPUT_JOYSTICK3              41306

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        107
#define _APS_NEXT_COMMAND_VALUE         41304
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

// menu positions

// GIJUTSU-HYORON-SHA Babbase-2nd
#ifdef _BABBAGE2ND
#define MENU_POS_CONTROL                0
#define MENU_POS_BINARY1                1
#endif

// HITACHI BASIC Master Jr
#ifdef _BMJR
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// TOSHIBA EX-80
#ifdef _EX80
#define MENU_POS_CONTROL                0
#define MENU_POS_BINARY1                1
#define MENU_POS_TAPE                   2
#define MENU_POS_SOUND                  3
#endif

// Nintendo Family BASIC
#ifdef _FAMILYBASIC
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// FUJITSU FM-8
#ifdef _FM8
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// FUJITSU FM-7
#ifdef _FM7
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// FUJITSU FM-77
#ifdef _FM77
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// FUJITSU FM77AV
#ifdef _FM77AV
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// FUJITSU FM77AV40
#ifdef _FM77AV40
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// FUJITSU FM77AV40EX
#ifdef _FM77AV40EX
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// FUJITSU FM16pi
#ifdef _FM16PI
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// FUJITSU FMR-30
#ifdef _FMR30
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// FUJITSU FMR-50
// FUJITSU FMR-CARD
#ifdef _FMR50
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// FUJITSU FMR-60
#ifdef _FMR60
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// CASIO FP-200
#ifdef _FP200
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// CASIO FP-1100
#ifdef _FP1100
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// SEGA GAME GEAR
#ifdef _GAMEGEAR
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_FD1                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// EPSON HC-20
#ifdef _HC20
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// EPSON HC-40
#ifdef _HC40
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_TAPE                   5
#define MENU_POS_SCREEN                 6
#define MENU_POS_SOUND                  7
#define MENU_POS_INPUT                  8
#endif

// EPSON HC-80
#ifdef _HC80
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// National JR-100
#ifdef _JR100
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// TOSHIBA J-3100GT
#ifdef _J3100GT
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// TOSHIBA J-3100SL
#ifdef _J3100SL
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// IBM Japan Ltd PC/JX
#ifdef _JX
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// SORD m5
#ifdef _M5
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_TAPE                   2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// SEIKO MAP-1010
#ifdef _MAP1010
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// SEGA MASTER SYSTEM
#ifdef _MASTERSYSTEM
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// ASCII MSX1
#ifdef _MSX1
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_CART2                  2
#define MENU_POS_TAPE                   3
#define MENU_POS_FD1                    4
#define MENU_POS_FD2                    5
#define MENU_POS_SCREEN                 6
#define MENU_POS_SOUND                  7
#define MENU_POS_INPUT                  8
#endif

// ASCII MSX2
#ifdef _MSX2
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_CART2                  2
#define MENU_POS_TAPE                   3
#define MENU_POS_FD1                    4
#define MENU_POS_FD2                    5
#define MENU_POS_SCREEN                 6
#define MENU_POS_SOUND                  7
#define MENU_POS_INPUT                  8
#endif

// MITSUBISHI Elec. MULTI8
#ifdef _MULTI8
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// Japan Electronics College MYCOMZ-80A
#ifdef _MYCOMZ80A
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// SHARP MZ-80A
#ifdef _MZ80A
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_FD1                    2
#define MENU_POS_FD2                    3
#define MENU_POS_FD3                    4
#define MENU_POS_FD4                    5
#define MENU_POS_SCREEN                 6
#define MENU_POS_SOUND                  7
#define MENU_POS_INPUT                  8
#endif

// SHARP MZ-80B
#ifdef _MZ80B
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_FD1                    2
#define MENU_POS_FD2                    3
#define MENU_POS_FD3                    4
#define MENU_POS_FD4                    5
#define MENU_POS_SCREEN                 6
#define MENU_POS_SOUND                  7
#define MENU_POS_INPUT                  8
#endif

// SHARP MZ-80K
#ifdef _MZ80K
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_FD1                    2
#define MENU_POS_FD2                    3
#define MENU_POS_FD3                    4
#define MENU_POS_FD4                    5
#define MENU_POS_SCREEN                 6
#define MENU_POS_SOUND                  7
#define MENU_POS_INPUT                  8
#endif

// SHARP MZ-700
#ifdef _MZ700
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// SHARP MZ-800
#ifdef _MZ800
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_QD1                    2
#define MENU_POS_FD1                    3
#define MENU_POS_FD2                    4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// SHARP MZ-1200
#ifdef _MZ1200
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_FD1                    2
#define MENU_POS_FD2                    3
#define MENU_POS_FD3                    4
#define MENU_POS_FD4                    5
#define MENU_POS_SCREEN                 6
#define MENU_POS_SOUND                  7
#define MENU_POS_INPUT                  8
#endif

// SHARP MZ-1500
#ifdef _MZ1500
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_QD1                    2
#define MENU_POS_FD1                    3
#define MENU_POS_FD2                    4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// SHARP MZ-2200
#ifdef _MZ2200
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_QD1                    2
#define MENU_POS_FD1                    3
#define MENU_POS_FD2                    4
#define MENU_POS_FD3                    5
#define MENU_POS_FD4                    6
#define MENU_POS_SCREEN                 7
#define MENU_POS_SOUND                  8
#define MENU_POS_INPUT                  9
#endif

// SHARP MZ-2500
#ifdef _MZ2500
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_TAPE                   5
#define MENU_POS_SCREEN                 6
#define MENU_POS_SOUND                  7
#define MENU_POS_INPUT                  8
#endif

// SHARP MZ-2800
#ifdef _MZ2800
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// SHARP MZ-3500
#ifdef _MZ3500
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// SHARP MZ-5500
#ifdef _MZ5500
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// SHARP MZ-6500
#ifdef _MZ6500
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// SHARP MZ-6550
#ifdef _MZ6550
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// NEC N5200
#ifdef _N5200
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// TOSHIBA PASOPIA
#ifdef _PASOPIA
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_BINARY1                4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// TOSHIBA PASOPIA7
#ifdef _PASOPIA7
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_BINARY1                4
#define MENU_POS_BINARY2                5
#define MENU_POS_SCREEN                 6
#define MENU_POS_SOUND                  7
#define MENU_POS_INPUT                  8
#endif

// NEC PC-6001
#ifdef _PC6001
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_FD1                    2
#define MENU_POS_FD2                    3
#define MENU_POS_TAPE                   4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// NEC PC-6001mkII
#ifdef _PC6001MK2
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_FD1                    2
#define MENU_POS_FD2                    3
#define MENU_POS_TAPE                   4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// NEC PC-6001mkIISR
#ifdef _PC6001MK2SR
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_FD1                    2
#define MENU_POS_FD2                    3
#define MENU_POS_TAPE                   4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// NEC PC-6601
#ifdef _PC6601
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_FD1                    2
#define MENU_POS_FD2                    3
#define MENU_POS_FD3                    4
#define MENU_POS_FD4                    5
#define MENU_POS_TAPE                   6
#define MENU_POS_SCREEN                 7
#define MENU_POS_SOUND                  8
#define MENU_POS_INPUT                  9
#endif

// NEC PC-6601SR
#ifdef _PC6601SR
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_FD1                    2
#define MENU_POS_FD2                    3
#define MENU_POS_FD3                    4
#define MENU_POS_FD4                    5
#define MENU_POS_TAPE                   6
#define MENU_POS_SCREEN                 7
#define MENU_POS_SOUND                  8
#define MENU_POS_INPUT                  9
#endif

// NEC PC-8001mkIISR
#ifdef _PC8001SR
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// NEC PC-8201
#ifdef _PC8201
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// NEC PC-8201A
#ifdef _PC8201A
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// NEC PC-8801MA
#ifdef _PC8801MA
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// NEC PC-9801
#ifdef _PC9801
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_FD5                    5
#define MENU_POS_FD6                    6
#define MENU_POS_TAPE                   7
#define MENU_POS_SCREEN                 8
#define MENU_POS_SOUND                  9
#define MENU_POS_INPUT                  10
#endif

// NEC PC-9801E/F/M
#ifdef _PC9801E
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_FD5                    5
#define MENU_POS_FD6                    6
#define MENU_POS_TAPE                   7
#define MENU_POS_SCREEN                 8
#define MENU_POS_SOUND                  9
#define MENU_POS_INPUT                  10
#endif

// NEC PC-9801U
#ifdef _PC9801U
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// NEC PC-9801VF
#ifdef _PC9801VF
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// NEC PC-9801VM
#ifdef _PC9801VM
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// NEC PC-98DO
#ifdef _PC98DO
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_FD3                    3
#define MENU_POS_FD4                    4
#define MENU_POS_TAPE                   5
#define MENU_POS_SCREEN                 6
#define MENU_POS_SOUND                  7
#define MENU_POS_INPUT                  8
#endif

// NEC PC-98HA
#ifdef _PC98HA
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// NEC PC-98LT
#ifdef _PC98LT
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// NEC PC-100
#ifdef _PC100
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// NEC-HE PC Engine
#ifdef _PCENGINE
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// SANYO PHC-20
#ifdef _PHC20
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// SANYO PHC-25
#ifdef _PHC25
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// CASIO PV-1000
#ifdef _PV1000
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// CASIO PV-2000
#ifdef _PV2000
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_TAPE                   2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// PIONEER PX-7
#ifdef _PX7
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_CART2                  2
#define MENU_POS_TAPE                   3
#define MENU_POS_LASER_DISC             4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// TOMY PYUTA
#ifdef _PYUTA
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_TAPE                   2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// EPSON QC-10
#ifdef _QC10
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// BANDAI RX-78
#ifdef _RX78
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_TAPE                   2
#define MENU_POS_SCREEN                 3
#define MENU_POS_SOUND                  4
#define MENU_POS_INPUT                  5
#endif

// SEGA SC-3000
#ifdef _SC3000
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_FD1                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// EPOCH Super Cassette Vision
#ifdef _SCV
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// SONY SMC-777
#ifdef _SMC777
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif


// NEC TK-80BS (COMPO BS/80)
#ifdef _TK80BS
#define MENU_POS_CONTROL                0
#define MENU_POS_BINARY1                1
#define MENU_POS_TAPE                   2
#define MENU_POS_SOUND                  3
#endif

// CANON X-07
#ifdef _X07
#define MENU_POS_CONTROL                0
#define MENU_POS_TAPE                   1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

// SHARP X1
#ifdef _X1
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// SHARP X1twin
#ifdef _X1TWIN
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_CART1                  4
#define MENU_POS_SCREEN                 5
#define MENU_POS_SOUND                  6
#define MENU_POS_INPUT                  7
#endif

// SHARP X1turbo
#ifdef _X1TURBO
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// SHARP X1turboZ
#ifdef _X1TURBOZ
#define MENU_POS_CONTROL                0
#define MENU_POS_FD1                    1
#define MENU_POS_FD2                    2
#define MENU_POS_TAPE                   3
#define MENU_POS_SCREEN                 4
#define MENU_POS_SOUND                  5
#define MENU_POS_INPUT                  6
#endif

// SHINKO SANGYO YS-6464A
#ifdef _YS6464A
#define MENU_POS_CONTROL                0
#define MENU_POS_BINARY1                1
#endif

// Homebrew Z80 TV GAME SYSTEM
#ifdef _Z80TVGAME
#define MENU_POS_CONTROL                0
#define MENU_POS_CART1                  1
#define MENU_POS_SCREEN                 2
#define MENU_POS_SOUND                  3
#define MENU_POS_INPUT                  4
#endif

